%if 0%{?fedora}
%global with_python3 1
%endif

%define srcname workdays

Name:           python-%{srcname}
Version:        1.4
Release:        1%{?dist}
Summary:        Workday date utility functions to extend python's datetime
License:        BSD
URL:            http://github.com/ogt/workdays
Source0:        https://pypi.python.org/packages/source/w/%{srcname}/%{srcname}-%{version}.tar.gz


BuildRequires:  python-devel >= 2.6
BuildRequires:  python-setuptools
%if 0%{?with_python3}
BuildRequires:  python3-devel
%endif # with_python3

BuildArch:      noarch

%description
Extend python datetime with excel-like workday addition/subtraction


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

%description -n python2-%{srcname}
Extend python datetime with excel-like workday addition/subtraction

%if 0%{?with_python3}
%package -n python3-%{srcname}
Summary:        %{summary}
Requires:       python3
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname}
Extend python datetime with excel-like workday addition/subtraction
%endif # with_python3

%prep
%autosetup -n %{srcname}-%{version} -p1

%build
%{py2_build}

%if 0%{?with_python3}
%{py3_build}
%endif # with_python3

%install
%py2_install

%if 0%{?with_python3}
%py3_install
%endif # with_python3

%files -n python2-%{srcname}
%{python_sitelib}/*

%if 0%{?with_python3}
%files -n python3-%{srcname}
%{python3_sitelib}/*
%endif # with_python3


%changelog
* Fri Apr 07 2017 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.4-1
- initial package
